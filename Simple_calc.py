import numpy as np
import collections
from scipy.signal import butter, lfilter, freqz



def distance2D(x,y):
	return ((x[1:]-x[0:-1])**2+(y[1:]-y[0:-1])**2)**0.5

def velocity2D(x=[],y=[],t=[]):
	return (((x[1:]-x[0:-1])**2+(y[1:]-y[0:-1])**2)**0.5)/(t[1:]-t[0:-1])


def findduplicate(a):
	return [item for item, count in collections.Counter(a).items() if count > 1]

def first_index_gt(data_list, value):
    '''return the first index greater than value from a given list like object'''
    try:
        index = next(data[0] for data in enumerate(data_list) if data[1] > value)
        return index
    except StopIteration: return - 1

def first_index_lt(data_list, value):
    '''return the first index less than value from a given list like object'''
    try:
        index = next(data[0] for data in enumerate(data_list) if data[1] < value)
        return index
    except StopIteration: return - 1

def first_index_ne(data_list, value):
    '''returns first index not equal to the value from list'''
    try:
        index = next(data[0] for data in enumerate(data_list) if data[1] != value)
        return index
    except StopIteration: return - 1

def first_index_et(data_list, value):
    '''same as data_list.index(value), except with exception handling
    Also finds 'nan' values'''
    try:
        if type(value) == float and math.isnan(value):
            return next(data[0] for data in enumerate(data_list)
              if (type(data[1]) in (float, np.float64, np.float32, np.float96)
              and math.isnan(data[1])))
        else:
            return next(data[0] for data in enumerate(data_list) if data[1] == value)
    except (ValueError, StopIteration): return - 1

def del_list_numpy(l, id_to_del):
    arr = np.array(l)
    return list(np.delete(arr, id_to_del))


def transposelist(listData):
	return list(map(list, zip(*listData)))


def downsampling(data_in, pre_rate=20000,post_rate=100):
    data_out = []
    for data in data_in:
        bin_data = data.reshape(-1, int(pre_rate/post_rate)).mean(axis=1)
        data_out.append(bin_data/np.mean(bin_data))
    return data_out


def rescaleData(data_in, mindata=0, maxdata=1):
    data_out = []
    for data in data_in:
        scdata = np.interp(data, (data.min(), data.max()), (mindata, maxdata))
        data_out.append(scdata)
    return data_out

# pass filter
def butter_lowpass(data, cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='lowpass', analog=False)
    y = lfilter(b, a, data)
    return y

def butter_highpass(data, cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='highpass', analog=False)
    y = lfilter(b, a, data)
    return y

def smooth(x,window_len=11):
    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    w=np.ones(window_len,'d')
    y=np.convolve(w/w.sum(),s,mode='valid')
    return y

def smooth_conv(x,window_len=11,window='flat'):
    """
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.
    output:
        the smoothed signal
    """

    if x.ndim != 1:
        raise ValueError ("smooth only accepts 1 dimension arrays.")
    if x.size < window_len:
        raise ValueError ("Input vector needs to be bigger than window size.")
    if window_len<3:
        return x
    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError ("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")
    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')
    y=np.convolve(w/w.sum(),s,mode='valid')
    return y[int((window_len-1)/2):-int((window_len-1)/2+1)]

def SlideWindowData(binning_signalall, binning_rates, pretime,posttime,binning_time,rescaleinput = True):
    size_N = len(binning_rates)
    bin_pre = round(pretime/binning_time) 
    bin_post  = round(posttime/binning_time)  
    WindowSize = bin_pre+bin_post+1. # W length
    TotalListNum = len(binning_signalall)- WindowSize + 1. # maximun list number of the data 
    # # smooth stimulus or not 
    # remove the extra stimulation to fit data length
    if rescaleinput == True:
        binning_signalall =rescaleData([binning_signalall], mindata=0, maxdata=1).flatten()

    if bin_post ==0:
        targetsignal = binning_signalall[bin_pre:]/max(binning_signalall[bin_pre:]) 
    else:
        targetsignal = binning_signalall[bin_pre:-bin_post]/max(binning_signalall[bin_pre:-bin_post]) 
    # convert the continuous data to a list, each element of the list is a windowed data 
    print('list data')
    ListData = []
    for k in range(len(targetsignal)):
        StartInd = k 
        EndInd = k + bin_pre + bin_post +1
        windowdata = []
        for i in range(size_N):
            windowdata.extend(binning_rates[i][StartInd:EndInd])
        ListData.append(windowdata) # append windowdata to a list, the len() should be the same with binning_signal
    FiringData = np.array(ListData)
    return FiringData, targetsignal

def ListToArray(a):
    # fill with 0 to the same length
    b = np.zeros([len(a),len(max(a,key = lambda x: len(x)))])
    for i,j in enumerate(a):
        b[i][0:len(j)] = j
    return b
    # plt.eventplot(ListToArray(predspk), colors='k')

def slidwindow(iterable, windowsize):
    i = iter(iterable)
    win = []
    for e in range(0, windowsize):
        win.append(next(i))
    yield win
    for e in i:
        win = win[1:] + [e]
        yield win

def rolling_window(a, size):
    shape = a.shape[:-1] + (a.shape[-1] - size + 1, size)
    strides = a.strides + (a. strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def mi_quick(a,b,d,bn=32):
    if d>0: xy,_,_ = np.histogram2d(a[d:],b[:-d],bn)
    elif d<0: xy,_,_ = np.histogram2d(a[:d],b[-d:],bn)
    else: xy,_,_ = np.histogram2d(a,b,bn)
    xy /= np.sum(xy)
    px = [np.array([max(x,1e-100) for x in np.sum(xy,axis=0)])]
    py = np.transpose([[max(x,1e-100) for x in np.sum(xy,axis=1)]])
    nxy = (xy/px)/py
    nxy[nxy==0] = 1e-100
    return np.sum(xy*np.log2(nxy))
    # dms = range(-100,101)
    # mitrainsf = [mi_quick(y_train[:,0],y_train[:,0],d) for d in dms]


def half_corr(signal, binning=10):
	autocorrall = np.correlate(signal,signal,"same")
	norm_autocorrall = autocorrall/max(autocorrall)
	halfcorr = np.argmin(abs(norm_autocorrall[int((len(norm_autocorrall)+1)/2):]-0.5))*binning
	return halfcorr


def EqualMI(data1,data2,binnum=32):
    sortdata1 = np.sort(data1.flatten())
    binedge1 = sortdata1[np.append(np.array(list(range(binnum)))*int(len(data1)/binnum),int(len(data1))-1)]
    sortdata2 = np.sort(data2.flatten())
    binedge2 = sortdata2[np.append(np.array(list(range(binnum)))*int(len(data2)/binnum),int(len(data2))-1)]
    xy,_,_ = np.histogram2d(data1.flatten(),data2.flatten(),bins=(binedge1, binedge2))
    xy /= np.sum(xy)
    px = [np.array([max(x,1e-100) for x in np.sum(xy,axis=0)])]
    py = np.transpose([[max(x,1e-100) for x in np.sum(xy,axis=1)]])
    nxy = (xy/px)/py
    nxy[nxy==0] = 1e-100
    return np.sum(xy*np.log2(nxy))

    
def mi(a,b,bn=32):
    xy,_,_ = np.histogram2d(a,b,bn)
    xy /= np.sum(xy)
    px = [np.array([max(x,1e-100) for x in np.sum(xy,axis=0)])]
    py = np.transpose([[max(x,1e-100) for x in np.sum(xy,axis=1)]])
    nxy = (xy/px)/py
    nxy[nxy==0] = 1e-100
    return np.sum(xy*np.log2(nxy))


def quick_fft(data,sampling = 1000 ):
    import scipy.fftpack
    # Number of samplepoints
    N = len(data)
    # sample spacing
    T = 1.0 / sampling
    x = np.linspace(0.0, N*T, N)
    y = data
    yf = scipy.fftpack.fft(y)
    xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
    fftx = xf
    ffty = 2.0/N * np.abs(yf[:N//2])
    return fftx, ffty

def kth_diag_indices(pos,CNNH=52,CNNW=52):
    rows, cols = np.indices((CNNH,CNNW))
    row_idxs = np.diag(rows, k=pos)
    col_idxs = np.diag(cols, k=pos)
    return row_idxs, col_idxs