# dynamics of the stimuli
from random import gauss
import numpy as np
from random import shuffle
import datetime
from PIL import Image
import scipy.io
import scipy
import os
import random

# 

def reversedbar(x=[],CNNW=52,CNNH=52,direction ='RL'):
    if x ==[]:
        x = (list(range(CNNW))+list(range(CNNH))[::-1])*60
    imgback = np.zeros((len(x),CNNW,CNNH))
    for i,pos in enumerate(x.astype('int16')) :
        if direction =='RL': # right-left
            imgback[i,:,pos]=1 
        elif direction =='UD':
            imgback[i,pos,:]=1
        elif direction =='URDL': 
            dumpimg = np.zeros((CNNH,CNNW))
            row_idxs, col_idxs = kth_diag_indices((52-pos)*2-CNNW)
            dumpimg[row_idxs, col_idxs]=1
            imgback[i,:,:]=np.flip(dumpimg,1)
        elif direction =='ULDR':
            dumpimg = np.zeros((CNNH,CNNW))
            row_idxs, col_idxs = kth_diag_indices((52-pos)*2-CNNW)
            dumpimg[row_idxs, col_idxs]=1
            imgback[i,:,:]=dumpimg 
        else:
            print('please define the direction')
    return x, imgback


def checkbox(Nimg = 10000,CNNW=52,CNNH=52):
    imgback = np.zeros((Nimg,CNNW,CNNH))
    idex = [1]*int((CNNW*CNNH)/32)+[0]*(int((CNNW*CNNH)/32)+1)
    for i in range(Nimg) :
        shuffle(idex)
        checker = np.kron(np.reshape(idex,(int(CNNW/4),int(CNNH/4))),np.ones((4,4), dtype=int))
        imgback[i,:,:]= checker
    return imgback
    

def onoffsti(data):      
    imgback = np.zeros((len(data),CNNW,CNNH))
    for i in range(len(data)) :
        idex = [data[i]]*CNNW*CNNH
        checker = np.reshape(idex,(CNNW,CNNH))
        imgback[i,:,:]= checker
    return imgback


def HMM(Delta = 0.1, Gamma = 0.2, sDD = 0.01,dt=0.01):
    # Generate HMM time series
    omega2 = (Gamma/(2*1.06))**2
	xs = []
	vs = []
	x = 0
	v = 0
	tt = [] # time index
	for i in range(20000):
		tt.append(i)
		tp = x
		x = x+v*dt
		v = (1-Gamma*dt)*v-omega2*tp*dt+gauss(0,sDD)*(Delta*dt)**0.5
		xs.append(x)
		vs.append(v)
	return xs,vs


def OU(Delta = 0.1,Gamma = 15.0,sDD = 0.1,dt=0.01):
	xs = []
	x = 0 
	for i in range(10000):
		tp = x
		x = (1-dt*Gamma/(2.12)**2)*x+gauss(0,sDD)*(Delta*dt)**0.5
		xs.append(x)
	return xs



