import numpy as np
import scipy.io
import os
from os import walk
from os.path import join
import sys
import random
from os import listdir



# __all__ = ['listfile','readmat_SpkAndSti','readmat_RateAndSti','loadWh5','loadMCSh5']

def listfile(mypath,cond1='HMM',cond2='OU',filetype='mat'):
    allfile = []
    for path, dirs, files in walk(mypath):
        for name in files:
            file= join(path, name)
            if ((cond1 in name.split(".")[0]) or (cond2 in name.split(".")[0])) & (name.split(".")[-1]== filetype):
                print(file)
                allfile.append(file)
    return allfile

def readmat_SpkAndSti(file):
    # for spike time and stimulus
    mat = scipy.io.loadmat(file)
    TimeStamps = mat.get('TimeStamps')
    spikes=mat.get('Spikes')
    stimulation=mat.get('a_data')
    Info = mat.get('Infos')
    DataLength = np.float32(Info[0][0][7][0][0])/1000 # sec
    sampling = np.float32(Info[0][0][1][0][0]) #Hz

    # call out data
    size_N = 60
    channel_data = []
    for i in range(size_N):
        data=spikes[0][i][0] 
        channel_data.append(data)   

    # call out stimulus
    signal = stimulation[2]
    
    return channel_data,signal, TimeStamps, sampling


def readmat_RateAndSti(file,binning_time = 0.01,T_len=[5,285]):
    # for firing rate and stimulus
    # open matlab file 
    from package_Tina.DataManipulate import butter_lowpass

    mat = scipy.io.loadmat(file)
    TimeStamps = mat.get('TimeStamps')[0]
    spikes=mat.get('Spikes')
    stimulation=mat.get('a_data')
    Info = mat.get('Infos')
    sampling = 20000 # Hz
    triggerind = np.float16(TimeStamps[0]) # initial point, unit:s
    if T_len == []:
        T_span=TimeStamps
    else :
        T_span = [triggerind+T_len[0],triggerind+T_len[1]] # unit: s
    size_N = spikes.shape[1]
    
    
    # read stimulus
    signal = stimulation[2]
    signalfiltered = butter_lowpass(signal, 50, sampling, order=5)
    signalused = signalfiltered[int(T_span[0]*sampling):int(T_span[1]*sampling)] # take the useful time period
    temp_signal = signalused.reshape(-1, int(binning_time*sampling)).mean(axis=1) # downsampling by average
    binning_signalall = (temp_signal - np.mean(temp_signal))/max(temp_signal - np.mean(temp_signal)) # rescale
    
    # spikeing timestamps
    channel_data = []
    for i in range(size_N):
        data=spikes[0][i][0] 
        channel_data.append(data) 
    # calculate the firing rate
    N_bin = int((T_span[1]-T_span[0])/binning_time)+1 # number of bins
    bins = np.linspace(T_span[0], T_span[1], num=N_bin) # time point of data 
    binning_rates=[]
    for i in range(size_N):
        out = np.histogram(channel_data[i], bins)
        binning_rates.append(out[0]) 
    return binning_signalall, np.array(binning_rates)



def loadWh5(file):
    from keras.models import load_model
    model1 = load_model(file)
    for layer in model1.layers:    
        g1=layer.get_config()
        h1=layer.get_weights()
    Wmatrix = h1[0].reshape(60, -1)
    return Wmatrix


def ch_layout(insize = [50,50],outsize=[8,8],startpoint='middle',role='regular',binningrate=[]):
    # role='regular', 'random', 'max'
    # startpoint='middle', 'left'
    inlen = list(range(insize[0]*insize[1]))
    outlen = list(range(outsize[0]*outsize[1]))
    if role == 'regular':
        spacingx = int(insize[0]/(outsize[0]-1))
        spacingy = int(insize[1]/(outsize[1]-1))
        xpos = np.linspace(0,outsize[0],num=outsize[0],endpoint=False)*spacingx
        ypos = np.linspace(0,outsize[1],num=outsize[1],endpoint=False)*spacingy
        if startpoint=='middle':
            xpos = np.int_(xpos+(insize[0]-xpos[-1])/2)
            ypos = np.int_(ypos+(insize[1]-ypos[-1])/2)
        xpos = np.repeat(xpos, 8)
        ypos = np.array(list(ypos)*8)
        xpos = np.delete(xpos,[0,7,56,63])
        ypos = np.delete(ypos,[0,7,56,63])
        channelID=[x*insize[0]+y for x,y in zip(xpos,ypos)]
    if (role == 'max') & (binningrate == []):
        sys.exit('input the binning rate')
    if (role == 'max') & (binningrate != []):
        channelID =np.argsort(sum(binningrate))[-60:]
        allindex = np.array(inlen)[channelID]
        xpos = np.int_(allindex / insize[0])
        ypos = np.int_(allindex % insize[1])
    if role == 'random':
        channelID =random.sample(range(2500), 60)
        allindex = inlen[channelID]
        xpos = np.int_(allindex / insize[0])
        ypos = np.int_(allindex % insize[1])
    return xpos,ypos, channelID
        

def saveMCSh5toDat(file, Nsplit=1):
    # These are the imports of the McsData module
    import sys, importlib
    import McsPy.McsData
    import McsPy.McsCMOS
    from McsPy import ureg, Q_
    channel_raw_data = McsPy.McsData.RawData(file)
    analog_stream_0 = channel_raw_data.recordings[0].analog_streams[0]
    datasize = channel_raw_data.recordings[0].analog_streams[0].channel_data.shape
    outlen = int(datasize[1]/Nsplit)
    filename = file.split('.')[0]
    for i in range(Nsplit):
        startid = i*outlen
        endid = i*outlen+(outlen-1)
        rawdata = analog_stream_0.channel_data[:,startid:endid]
        np.transpose(rawdata).astype('int16').tofile(file.split('.')[0]+'_'+str(i)+'.dat')
    print('save to ',file.split('.')[0])

def loadMCSh5(file, period=[], channel=[]):
    # These are the imports of the McsData module
    import sys, importlib
    import McsPy.McsData
    import McsPy.McsCMOS
    from McsPy import ureg, Q_
    channel_raw_data = McsPy.McsData.RawData(file)
    analog_stream_0 = channel_raw_data.recordings[0].analog_streams[0]
    datasize = channel_raw_data.recordings[0].analog_streams[0].channel_data.shape
    if period==[]:
        startid=0
        endid=datasize[1]
    else:
        if period[1] > datasize[1]:
            sys.exit('out of range, the maximun data size is '+str(datasize[1]))       
        startid=period[0]
        endid=period[1]

    if channel==[]:
        rawdata = analog_stream_0.channel_data[:,startid:endid]
    else:
        if channel > (datasize[0]-1):
            sys.exit('out of range, the channel id is up to '+str(datasize[0]-1))
        rawdata = analog_stream_0.channel_data[channel,startid:endid]
    return rawdata


def Corem_read_Tina(file, simTime=600000, binsize=10, ncells=50*50):
    Coremdata = np.load(file)
    evs = Coremdata['evs']-1 # strating from  2
    ts = Coremdata['ts']+1
    nbins = [int(simTime/binsize),int(ncells)]
    ranges = [[0.5,int(simTime)+0.5],[0.5,int(ncells)+0.5]]
    binningrate = np.histogram2d(ts,evs,bins=nbins ,range=ranges)[0]
    return binningrate


def Corem_read_David(file, datalength=60000, binsize=10):
    Coremdata = np.load(file)
    binedge = np.linspace(0, datalength*binsize, num=datalength+1, endpoint=True)
    binningrate = np.array([np.histogram(Coremdata.item()[i],bins = binedge)[0].astype(float) for i in Coremdata.item()])
    # binningrate = np.array([gaussian_filter1d(np.histogram(Coremdata.item()[i],bins = binedge)[0].astype(float), sigma=1) for i in Coremdata.item()])
    return binningrate[2:627,:]


def offsusID(file):
    mat = scipy.io.loadmat(file)
    TimeStamps = mat.get('TimeStamps')[0]
    spikes=mat.get('Spikes')
    Info = mat.get('Infos')
    DataLength = np.float32(Info[0][0][7][0][0])/1000 # sec
    T_span=[0,DataLength]
    sampling = np.float32(Info[0][0][1][0][0]) # Hz
    size_N = 60
    binning_time = 0.01
    channel_data = []
    spkID=[]
    for i in range(size_N):
        data=spikes[0][i][0] 
        channel_data.append(data) 
        spkID.append([i]*len(data))
    # calculate the firing rate
    N_bin = int((T_span[1]-T_span[0])/binning_time)+1 # number of bins
    bins = np.linspace(T_span[0], T_span[1], num=N_bin) # time point of data 
    binning_rates=[]
    for i in range(size_N):
        out = np.histogram(channel_data[i], bins)
        binning_rates.append(out[0]) 
    rate = np.array(binning_rates)
    if len(TimeStamps) == 21:
        offid = np.delete(np.array(list(range(28))),np.array(list(range(7)))*4)
        onstamp2 = TimeStamps.copy()
        offstamp = onstamp2+2
    if len(TimeStamps) == 29:
        offid = np.delete(np.array(list(range(28))),np.array(list(range(7)))*4)
        onstamp = TimeStamps.copy()
        delID = list(range(3,29,4))
        onstamp2 = np.delete(np.delete(onstamp,0),delID)
        offstamp = onstamp2+2
    ontotal = []
    offtotal = []
    for j in rate:
        for ontime,offtime in zip(onstamp2/binning_time,offstamp/binning_time):
            onspks = sum(j[int(ontime):int(ontime)+200])
            offspks = sum(j[int(offtime):int(offtime)+200])
            ontotal.append(onspks)
            offtotal.append(offspks)
    chonspks = np.sum(np.reshape(ontotal,[60,-1]),axis=1)
    choffspks = np.sum(np.reshape(offtotal,[60,-1]),axis=1)
    OffSustainCh = np.where((choffspks>chonspks*5)&(choffspks>np.mean(choffspks)))[0]
    return OffSustainCh

def loadimage(imgfolderpath):
    from PIL import Image
    files = listdir(imgfolderpath)
    files.sort()
    if 'np' in imgfolderpath:
        allimage=[np.load(imgfolderpath+file)/255 for file in files]
    elif 'mat' in imgfolderpath:
        allimage=[np.array(scipy.io.loadmat(imgfolderpath+file).get('imgs'))/255 for file in files]
    else:
        allimage=[np.array(Image.open(imgfolderpath+file))/255 for file in files]
    return  allimage

def batchloadimage(imgfolderpath, N_image=1000, loopcount=0):
    from PIL import Image
    allfiles = listdir(imgfolderpath)
    allfiles.sort()
    files = allfiles[(N_image*loopcount) : N_image*(loopcount+1)]
    if 'np' in imgfolderpath:
        batchimage=[np.load(imgfolderpath+file)/255 for file in files]
    else:
        batchimage=[np.array(Image.open(imgfolderpath+file))/255 for file in files]
    return  batchimage


# from stiimg import loadimage
# imgfolderpath = '/home/tina/disk/TensorflowCodeTina/CoremTest/2018_08_01_npimg/'
# allimage = loadimage(imgfolderpath)
# print('allimage')
# imgstack = np.transpose([allimage[i:-(40-i)] for i in range(40)],axes=[1,2,3,0])
# print('imgstack')

def Ronamat_read(file,binning_time = 0.01):
    # open matlab file 
    mat = scipy.io.loadmat(file)
    TimeStamps = mat.get('TimeStamps')[0]
    spikes=mat.get('Spikes')
    stimulation=mat.get('a_data')
    Info = mat.get('Infos')
    DataLength = np.float32(Info[0][0][7][0][0])/1000 # sec
    sampling = np.float32(Info[0][0][1][0][0]) # Hz
    triggerind = np.float16(TimeStamps[0]) # initial point, unit:s
    T_span = [triggerind+5,triggerind+285] # unit: s
    size_N = 60
    timedata = np.linspace(1., DataLength*sampling, num=int(DataLength*sampling))/sampling
    # read stimulus
    signal = stimulation[2]
    signalfiltered = butter_lowpass(signal, 50, sampling, order=5)
    signalused = signalfiltered[int(T_span[0]*sampling):int(T_span[1]*sampling)] # take the useful time period
    temp_signal = signalused.reshape(-1, int(binning_time*sampling)).mean(axis=1) # downsampling by average
    binning_signalall = (temp_signal - np.mean(temp_signal))/max(temp_signal - np.mean(temp_signal)) # rescale
    # spikeing timestamps
    channel_data = []
    for i in range(size_N):
        data=spikes[0][i][0] 
        channel_data.append(data) 
    # calculate the firing rate
    N_bin = int((T_span[1]-T_span[0])/binning_time)+1 # number of bins
    bins = np.linspace(T_span[0], T_span[1], num=N_bin) # time point of data 
    binning_rates=[]
    for i in range(size_N):
        out = np.histogram(channel_data[i], bins)
        binning_rates.append(out[0]) 
    return binning_signalall, binning_rates, size_N, T_span, binning_time


def get_layer_output(model, model_inputs, outputlayer=3, training_flag=False, plot_flag=False, batchnum=0, kernelnum=0):
    from keras import backend as K
    get_output = K.function([model.layers[0].input, K.learning_phase()],[model.layers[outputlayer].output])
    layer_output = get_output([model_inputs, training_flag])[0]
    if plot_flag:
        plt.imshow(outs[batchnum,:,:,kernelnum])
        plt.show()
    return layer_output
# outputs = [layer.output for layer in model.layers]          # all layer outputs
# functors = [K.function([inp]+ [K.learning_phase()], [out]) for out in outputs] # evaluation functions

def get_kernel(model, layerID=0, channelID=0, kernelID=0):
    modelconfig=model.get_config()
    modelweight=model.get_weights()
    tempkernel = np.squeeze(modelweight[layerID][:,:,channelID,kernelID])
    return tempkernel


