# functions for simulate a retina

import numpy as np

__all__=['ST_filter','PoissonSpikeGen_CJJ']

def s_filter(Ngrid = 50j,ind = 3,outd = 1.5):
	#spatial filter
	#ind = inside diameter, outd = outside diameter
	x1, y1 = (np.mgrid[-ind:ind:Ngrid, -ind:ind:Ngrid])
	gaussian_kernel_up = np.exp(-(x1**2+y1**2))

	x2, y2 = (np.mgrid[-outd:outd:Ngrid, -outd:outd:Ngrid])
	gaussian_kernel_down = -np.exp(-(x2**2+y2**2))

	gaussian_kernel_temp = gaussian_kernel_up + gaussian_kernel_down*0.5
	spatial_filter = gaussian_kernel_temp / gaussian_kernel_temp.max()

	return spatial_filter


def t_filter(temporal_len = 40):
	#temporal filter
	dt_kernel = 200/temporal_len
	t = np.arange(0, temporal_len * dt_kernel, dt_kernel) # Time vector 
	K1 = 1.05
	K2 = 0.7
	c1 = 0.14
	c2 = 0.12
	n1 = 7.0
	n2 = 8.0
	t1 = -6.0
	t2 = -6.0
	td = 6.0

	p1 = K1 * ((c1*(t - t1))**n1 * np.exp(-c1*(t - t1))) / ((n1**n1) * np.exp(-n1))
	p2 = K2 * ((c2*(t - t2))**n2 * np.exp(-c2*(t - t2))) / ((n2**n2) * np.exp(-n2))
	p3 = p1 - p2 

	temporal_filter = p3/p3.max()
	return temporal_filter

def ST_filter(Ngrid = 50j,ind = 3,outd = 1.5,temporal_len = 40):
	spatial_filter = s_filter(Ngrid = Ngrid,ind = ind,outd = outd)
	temporal_filter = t_filter(temporal_len = temporal_len)

	ST_filter = np.zeros((50, 50,40))
	for k, p3 in enumerate(temporal_filter): 
		ST_filter[...,k] = p3 * spatial_filter 
	return ST_filter, spatial_filter, temporal_filter

def PoissonSpikeGen_CJJ(rate, dt=0.01):
    # the code from CJJ, in IOP, sinica.
    # generate spike train
    i = 0
    s = 0
    spks = []
    while True:
        s += -np.log(1-np.random.uniform())
        while s>rate[i]*dt:
            s -= rate[i]*dt
            i += 1
            if i>=len(rate): break
        else:
            spks.append((i+s/rate[i])*dt)
            continue
        break
    return np.array(spks)

def PoissonSpikeGen_Tina(rate,dt=0.001):
    value = 1.0 / dt
    output = np.zeros(rate.shape[0])
    
    def next_spike_times(rate):        
        return -np.log(1.0-np.random.RandomState().rand(rate.shape[0])) / rate

    next_spikes = next_spike_times(rate)
    s = np.where(next_spikes<dt)[0]
    count = len(s)
    output[s] += value
    while count > 0:
        next_spikes[s] += next_spike_times(rate[s])
        s2 = np.where(next_spikes[s]<dt)[0]
        count = len(s2)
        s = s[s2]
        output[s] += value
        
    return np.where(output)[0]*dt