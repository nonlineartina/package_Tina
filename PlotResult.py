import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
from itertools import count
import sys
from PIL import Image
import os
import datetime


# __all__ = ['PlotImageFiring','PlotTotalfiring','PlotChannelRaster','Show2DRFimage','STAplot','plot8by8','plotyy_Tina']
# compare with stimuli
def PlotImageFiring(imgstack,binningrate, init=30000,n_data = 1000):
	final = init+n_data
	f, (a0, a1) = plt.subplots(2,1, gridspec_kw = {'height_ratios':[9, 1]})
	plt.xlim(0, n_data)
	a0.imshow(binningrate[:,init+40:final+40], cmap='Greys')
	a0.tick_params(
		axis='x',          # changes apply to the x-axis
		which='both',      # both major and minor ticks are affected
		bottom='off',      # ticks along the bottom edge are off
		top='off',         # ticks along the top edge are off
		labelbottom='off')
	imagesum = np.sum(np.sum(np.sum(imgstack,axis=1),axis=1),axis=1)
	a1.plot(imagesum[init:final])
	plt.xlim(0, n_data)
	plt.subplots_adjust(wspace=0, hspace=0)
	plt.show()


# compare the firing rate of traget and CNN
def PlotTotalfiring(targetrate,CNNrate, init=30000,n_data = 1000):
	final = init+n_data
	fig, ax1 = plt.subplots()
	ax1.plot(np.sum(targetrate,axis=0)[init:final],'b',lw=2.5)
	ax1.set_xlabel('time (datapoint/10ms)', fontsize='16')
	ax1.set_ylabel('target', color='b', fontsize='16')
	ax1.tick_params('y',colors='b')
	ax2 = ax1.twinx()
	ax2.plot((np.sum(CNNrate,axis=0)[init:final]-min(np.sum(CNNrate,axis=0)[init:final])),'r')
	ax2.set_ylabel('CNN', color='r', fontsize='16')
	ax2.tick_params('y',colors='r')
	plt.show()


def PlotChannelRaster(targetrate,CNNrate, init=30000,n_data = 1000):
	final = init+n_data
	for i in range(targetrate.shape[0]):
		fig, ax1 = plt.subplots()
		ax1.plot(targetrate[i,init:final],'b',lw=2.5)
		ax1.set_xlabel('time (datapoint/10ms)', fontsize='16')
		ax1.set_ylabel('target', color='b', fontsize='16')
		ax1.set_ylim([-0.1,1.1])
		ax1.tick_params('y',colors='b')
		ax2 = ax1.twinx()
		ax2.plot((CNNrate[i,init:final]>np.std(CNNrate[i]))*0.9,'r')
		ax2.set_ylabel('CNN', color='r', fontsize='16')
		ax2.set_ylim([-0.1,1.1])
		ax2.tick_params('y',colors='r')
		plt.show()


def Show2DRFimage(RFallimageCorem,RFallimageCNN):
	count = 0
	for i,j in zip(RFallimageCorem,RFallimage):
		print('cell ID = ',count)
		plt.subplot(1,2,1)
		plt.title('Target')
		plt.imshow(i[:,:])
		plt.subplot(1,2,2)
		plt.title('CNN')
		plt.imshow(j[:,:])
		plt.show()
		count += 1


def STAplot(channel_data,signal,TimeStamps,sampling,fileperiod):
	allSTA = temporalSTA(channel_data=channel_data,signal=signal, TimeStamps=TimeStamps, sampling=sampling)
	binSTA = downsampling(allSTA)
	fig, ax = plt.subplots()
	cax = ax.imshow(np.array(binSTA),extent=[-pretime,posttime,1,60], aspect='auto')
	cbar = fig.colorbar(cax)
	plt.show()


def plot8by8(data, dataLegend, imgsave=False, bgcolorID=[],bgcolorIDset=[], muteID= [], hlineloc=[], filename=[], filesubtitle=''):
    if len(data) != len(dataLegend):
        print("should be the same length of the dataLedend and the data")
    else:
        posind = np.double(list(range(64))).reshape(8,8).T.flatten().tolist()
        posind.remove(0)
        posind.remove(7)
        posind.remove(56)
        posind.remove(63)

        colorcode = ['b', 'r', 'g', 'c', 'm', 'y', 'k', 'w']
        bgcolorcode = ['peachpuff','palegreen','lightskyblue','lightgray']
        fig=plt.figure(figsize=(18, 16), dpi= 80, facecolor='w', edgecolor='k')

        for i in range(60):
            plt.subplot(8,8,posind[i]+1)
            ax = plt.gca()
            ax.axes.get_xaxis().set_visible(False)
            ax.axes.get_yaxis().set_visible(False)
            if i in muteID :
                pass
            else:                
                if i in bgcolorID:
                    ax.set_facecolor((0.9,0.9,0.9))
                for bgtype,bgset in enumerate(bgcolorIDset):
                    if i in bgset:
                        ax.set_facecolor(bgcolorcode[bgtype])
                plt.title(i)
                for j,channeldata, channellegend in zip(count(),data,dataLegend):
                    plt.plot(channeldata[i], colorcode[j] ,label=channellegend)
                    plt.vlines(len(channeldata[i])/2,min(channeldata[i]),max(channeldata[i]),linestyles='dotted')
                    if hlineloc != []:
                        plt.hlines(hlineloc,0,len(channeldata[i]),linestyles='dotted')

        legappend = []
        for k in range(len(data)):
            legappend.append(mpatches.Patch(color=colorcode[k], label=dataLegend[k]))
        fig.legend(handles=legappend, prop={'size': 16})

        if imgsave == True:
            plt.savefig(filename[:-4] + filesubtitle +'.png', bbox_inches='tight')#,transparent=True
            plt.clf()
            plt.close()
        else :
            plt.show()

def plotyy_Tina(data1,data2,xrange=[],label=['sti','CNN'],ax2legend =['RL','UD','URDL','ULDR'],axvline=[]):
    if len(data1) != len(data2):
        sys.exit('data have to be the same length')
    t=list(range(len(data1)))
    if xrange==[]:
        xrange=[0,len(data2)]
    fig, ax1 = plt.subplots()
    color = 'k'
    ax1.set_xlabel('time (s)', fontsize='16')
    ax1.set_ylabel(label[0], color=color, fontsize='16')
    ax1.plot(t, data1, color=color)
    ax1.set_xlim(xrange)
    ax1.tick_params(axis='y', labelcolor=color)
    color = 'tab:blue'
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel(label[1], color=color, fontsize='16')  # we already handled the x-label with ax1
    ax2.plot(t, data2)
    ax2.set_xlim(xrange)
    ax2.tick_params(axis='y', labelcolor=color)
    if ax2legend !=[]:
        ax2.legend(ax2legend)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    if axvline != []:
        plt.axvline(x=axvline,color='k',linestyle='dotted')
    plt.show()


def ShowChaLoc(arraysize,channelID=[],xpos=[],ypos=[],show=True):
	if (channelID==[])& ((xpos==[]) or (ypos==[])):
		sys.exit('input channelID, or, xpos and ypos')
	if channelID != []:
		inlen = list(range(arraysize[0]*arraysize[1]))
		allindex = np.array(inlen)[channelID]
		xpos = np.int_(allindex / arraysize[0])
		ypos = np.int_(allindex % arraysize[1])
	if xpos.shape != ypos.shape:
		sys.exit('xpos and ypos have to be the same shape')
	if (max(xpos) > arraysize[0]) or (max(ypos) > arraysize[1]):
		sys.exit('xpos and ypos bigger than arraysize ???')
	imgbk=np.zeros(arraysize)
	for x,y in zip(xpos,ypos):
		imgbk[x,y]+=1
	if show == True:
		plt.imshow(imgbk)
		plt.show()
	else:
		return imgbk

def saveSeqImg(data,outimgpath,subname='stiimg'):
	os.chdir(outimgpath)
	expday = datetime.datetime.now().strftime("%Y_%m_%d")
	if not os.path.exists(expday+'_'+subname):
		os.makedirs(expday+'_'+subname)
	outimgpath = os.getcwd()+'/'+expday+'_'+subname
	for count, img in enumerate(data):
		imgname = outimgpath+'/'+ subname +'%05d' % count+'.png'		
		im = Image.fromarray(img.astype('uint8'))
		im.save(imgname)


def PlotW(model,nchannel = 60):
	for layer in model.layers:    
	    g=layer.get_config()
	    h=layer.get_weights()
	Wmatrix = h[0].reshape(nchannel, -1)
	fig, ax = plt.subplots()
	cax = ax.imshow(Wmatrix, aspect='auto')
	cbar = fig.colorbar(cax)
	plt.title('coupling matrix')
	plt.show()


def PlotRaster(rate):
	plt.figure(figsize=(15,5))
	plt.imshow(rate,interpolation='nearest', aspect='auto',cmap='gray_r')
	plt.title('predict raster : testing set')
    plt.xlabel('time (datapoint/10ms)', fontsize='16')
    plt.ylabel('channel ID', color='k', fontsize='16')
	plt.show()

def PlotSpkRaster(rate,stdratio = 1):
    spks = []
    for i in rate:
        spks.append((i>np.std(i)*stdratio)*1)
    plt.figure(figsize=(15,5))
    plt.imshow(np.array(spks),interpolation='nearest', aspect='auto',cmap='gray_r')
    #   plt.title('predict raster : testing set')
    plt.show()


def PlotunidirCij(Cijin,nodetype = [],delidx = [],nodesize=[],imgsave=True):
    import networkx as nx
    # Cij : the coupling matrix
    # nodetype : can label the type of channel with different color, format : list of list
    # delidx : if u want delete the node
    # nodesize: change the size of node to present he other parameter such as firing rate etc.
    # create a graph
    if Cijin.shape == (60,60):
        insertidx= (0, 6, 54, 60)
        Cij = np.insert(np.insert(Cijin,insertidx,0,axis = 0),insertidx,0,axis = 1)
    else:
        Cij = Cijin
    G=nx.Graph()
    W = int(np.sqrt(Cij.shape[0]))
    H = int(np.sqrt(Cij.shape[1]))
    # add node
    for i in range(W*H):
        G.add_node(i)
    # set the position of nodes
    grid = np.indices((W, H))
    gridx = grid[0].flatten()
    gridy = grid[1].flatten()
    for n in G:
        G.node[n]['pos'] = (gridx[n],gridy[n] )
    pos = {n: data['pos'] for n, data in G.nodes(data=True)}    
    # set the color of nodes
    colorcode = ['b','r','g','k']
    nodecolors=['k']*int(np.sqrt(Cij.shape[0]))*int(np.sqrt(Cij.shape[1]))
    if nodetype != []:
        for count, i in enumerate(nodetype):
            for j in i:
                nodecolors[j]=colorcode[count]
    # detect index  
    if delidx != []:
        for i in delidx:
            Cij[i,:]=0
            Cij[:,i]=0 
    ######################## lower triangle ################## 
    couplings = Cij[np.tril_indices(W*H,k=-1)]
    edgecolors = ['b']*len(couplings)
    if np.where(couplings<0)[0].size != 0:
        for i in np.where(couplings<0)[0]:
            edgecolors[i]='r'
    # connect
    Cxinds, Cyinds = np.tril_indices(W*H,k=-1)
    for Cxind,Cyind,edgecolor,coupling in zip(Cxinds,Cyinds,edgecolors,couplings):
        G.add_edge(Cxind,Cyind,color=edgecolor,weight=coupling)
    weights = [G[u][v]['weight'] for u,v in G.edges()]
    colors = [G[u][v]['color'] for u,v in G.edges()]
    # node size
    if nodesize == []:
        nodesize = [1]*W*H
    for i in delidx:
        nodesize[i]=0
    # output the graph
    plt.figure(figsize=(8,8))
    nx.draw(G.to_undirected(),pos,with_labels=True, node_color=nodecolors, edge_color=colors, alpha=0.8, width=weights, node_size=nodesize)
    plt.title('lowerCij',fontsize=15)
    plt.gca().invert_yaxis()
    if imgsave == True:
        plt.savefig(file[:-4] + '_lowerCij.png', bbox_inches='tight')#,transparent=True
        plt.clf()
        plt.close()
    else :
        plt.show()
    ######################## upper triangle ##################    
    couplings = Cij[np.triu_indices(W*H,k=1)]
    edgecolors = ['b']*len(couplings)
    if np.where(couplings<0)[0].size != 0:
        for i in np.where(couplings<0)[0]:
            edgecolors[i]='r'
     # connect
    Cxinds, Cyinds = np.triu_indices(W*H,k=1)
    for Cxind,Cyind,edgecolor,coupling in zip(Cxinds,Cyinds,edgecolors,couplings):
        G.add_edge(Cxind,Cyind,color=edgecolor,weight=coupling)
    weights = [G[u][v]['weight'] for u,v in G.edges()]
    colors = [G[u][v]['color'] for u,v in G.edges()]
    # node size
    if nodesize == []:
        nodesize = [1]*W*H
    for i in delidx:
        nodesize[i]=0
    # output the graph
    plt.figure(figsize=(8,8))
    nx.draw(G.to_undirected(),pos,with_labels=True, node_color=nodecolors, edge_color=colors, alpha=0.8, width=weights, node_size=nodesize)
    plt.title('upperCij',fontsize=15)
    plt.gca().invert_yaxis()
    if imgsave == True:
        plt.savefig(file[:-4] + '_upperCij.png', bbox_inches='tight')#,transparent=True
        plt.clf()
        plt.close()
    else :
        plt.show()


def PlotbidirCij(Cijin,nodetype = [],delidx = [],nodesize=[],imgsave=True):
    import networkx as nx
    if Cijin.shape == (60,60):
        insertidx= (0, 6, 54, 60)
        Cij = np.insert(np.insert(Cijin,insertidx,0,axis = 0),insertidx,0,axis = 1)
    else:
        Cij = Cijin
    G=nx.MultiDiGraph()
    W = int(np.sqrt(Cij.shape[0]))
    H = int(np.sqrt(Cij.shape[1]))
    Cij[np.diag_indices(W*H)]=0
    # add node
    for i in range(W*H):
        G.add_node(i)
    # set the position of nodes
    grid = np.indices((W, H))
    gridx = grid[0].flatten()
    gridy = grid[1].flatten()
    for n in G:
        G.node[n]['pos'] = (gridx[n],gridy[n] )
    pos = {n: data['pos'] for n, data in G.nodes(data=True)}
    # set the color of nodes
    colorcode = ['b','r','g','k']
    nodecolors=['k']*int(np.sqrt(Cij.shape[0]))*int(np.sqrt(Cij.shape[1]))
    if nodetype != []:
        for count, i in enumerate(nodetype):
            for j in i:
                nodecolors[j]=colorcode[count]
    # detect index            
    if delidx != []:
        for i in delidx:
            Cij[i,:]=0
            Cij[:,i]=0 
    # add edge         
    Cxinds, Cyinds = np.indices((W*H,W*H))
    couplings = Cij.flatten()
    edgecolors = ['b']*len(couplings)
    if np.where(couplings<0)[0].size != 0:
        for i in np.where(couplings<0)[0]:
            edgecolors[i]='r'
    # connect
    for count,(Cxind,Cyind,edgecolor,coupling) in enumerate(zip(Cxinds.flatten(),Cyinds.flatten(),edgecolors,couplings)):
        G.add_edge(Cxind,Cyind,color=edgecolor,weight=coupling)
    # node size
    if nodesize == []:
        nodesize = [1]*W*H
    for i in delidx:
        nodesize[i]=0
    # output the graph
    plt.figure(figsize=(8,8))
    nx.draw(G,pos,with_labels=True, node_color=nodecolors, edge_color=edgecolors, alpha=0.8, width=couplings, node_size=nodesize)
    plt.title('Cij',fontsize=15)
    plt.gca().invert_yaxis()
    if imgsave == True:
        plt.savefig(file[:-4] + '_Cij.png', bbox_inches='tight')#,transparent=True
        plt.clf()
        plt.close()
    else :
        plt.show()


def RateAndSpk(rate,spk):
	f, ax = plt.subplots(2, 1, figsize=(15,5))
	ax[0].plot(rate*10)
	ax[1].scatter(spk,np.zeros([len(xspk)]),s=1)
	# adjust subplot sizes
	gs = GridSpec(2, 1, height_ratios=[5, 1])
	for i in range(2):
	    ax[i].set_position(gs[i].get_position(f))
	    # ax[i].set_xlim([2500,6000])
	plt.show()


def MIplot(train_pred,y_train,test_pred,y_test):
    dms = range(-100,101)
    mitrainsf = [mi_quick(y_train[:,0],y_train[:,0],d) for d in dms]
    mitrainTP = [mi_quick(y_train[:,0],train_pred[:,0],d) for d in dms]
    mitestsf = [mi_quick(y_test[:,0],y_test[:,0],d) for d in dms]
    mitestTP = [mi_quick(y_test[:,0],test_pred[:,0],d) for d in dms]
    MIMax = 'MI Max in '+str(10*(mitrainsf.index(max(mitrainsf))-100))+'='+str(max(mitrainsf)*100)
    MItrain = 'MI estimate train in '+str(10*(mitrainTP.index(max(mitrainTP))-100))+'='+str(max(mitrainTP)*100)
    MItest = 'MI estimate test in '+str(10*(mitestTP.index(max(mitestTP))-100))+'='+str(max(mitestTP)*100)
    plt.plot(np.double(list(dms))*10,np.double(mitrainsf)*100,'k',label='train self')
    plt.plot(np.double(list(dms))*10,np.double(mitrainTP)*100,'r',label='train/predict')
    plt.plot(np.double(list(dms))*10,np.double(mitrainTP)*500,'--r',label='5*(train/predict)')    
    plt.plot(np.double(list(dms))*10,np.double(mitestsf)*100,'b',label='test self')
    plt.plot(np.double(list(dms))*10,np.double(mitestTP)*100,'g',label='test/predict')
    plt.plot(np.double(list(dms))*10,np.double(mitestTP)*500,'--g',label='5*(test/predict)')
    plt.text(-150, 300, MIMax, fontsize=14)
    plt.text(-150, 100, MItest, fontsize=14)
    plt.xlabel('time (ms)')
    plt.ylabel('MI (bit/s)')
    plt.legend()    
    plt.title('MI')
    plt.tight_layout()
    plt.show()
    return print('plot MI'+'\n'+MItrain+'\n'+MItest+'\n')

def plot2by3(w,nameid,imgsave = False):
    fig, axes = plt.subplots(nrows=2, ncols=3, gridspec_kw={'hspace': 0.4, 'wspace': 0.2},figsize=(20,11))
    plt.tight_layout()
    for count, (w,nameid,ax) in enumerate(zip(Wall,name,axes.ravel())):
        Wmatrix = w.reshape(60, -1)
        im = ax.imshow(Wmatrix, aspect='auto')
        ax.set_xlim([50,150])
        ax.set_title(nameid,fontsize = 26)
        ax.set_xlabel('time (ms)',fontsize = 20)
        ax.set_ylabel('channel ID',fontsize = 20)
        ax.set_xticks([50,100,150])
        ax.set_xticklabels(['-500','0','500'])
        ax.tick_params(axis="x", labelsize=20)
        ax.tick_params(axis="y", labelsize=20)
        fig.colorbar(im,ax=ax)
    if imgsave == True:
        plt.savefig(filename[:-4] + filesubtitle +'.png', bbox_inches='tight')#,transparent=True
        plt.clf()
        plt.close()
    else :
        plt.show()


def FineTuneSubplot(data):
    fig, axs = plt.subplots(nrows=4, ncols=4, figsize=(15, 6), facecolor='w', edgecolor='k')
    subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
    for count,i in enumerate(data):
        axs[count].plot(i)
        axs[count].set_ylim([0,1])
        if count < ActArea.shape[0]-1:
            axs[count].set_xticklabels(())
            axs[count].title.set_visible(False)
    plt.show()


